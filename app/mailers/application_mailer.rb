class ApplicationMailer < ActionMailer::Base
  default from: "noreply@example.com"
  layout 'mailer'
  
  def contact_message(user)
    @user = user
    mail(:to => @user.email, :subject => 'Mensagem de Contato')
  end
end
